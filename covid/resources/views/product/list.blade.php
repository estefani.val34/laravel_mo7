@extends('home')

@section('content')
    @isset($arrproducts)
    <div class="card">
        <div class="card-header text-center">
           List products
        </div>
        <div class="card-body">
            <table class="table table-hover">
                <tr class="thead-dark">
                    <th>Id</th>
                    <th>Name</th>
                    <th>Description</th>
                </tr>
                @foreach ($arrproducts as $product) 
                    <tr>
                        <td><a href="{{ url('/product/edit/' . $product->id) }}"/>{{ $product->id }}</a></td>
                        <td><a href="{{ url('/product/edit/' . $product->id) }}"/>{{ $product->name }}</a></td>
                        <td>{{ $product->description }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    @endisset
@stop

@section('message')
    @isset($message)
        <div class="alert alert-warning">
            {{ $message }}
        </div>
    @endisset
@stop